import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {
   //Graph g = new Graph ("G");


   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");

      //g.createRandomSimpleGraph (6, 9);
      g.createRandomSimpleGraph (0, 0);

      System.out.println(g);
      System.out.println(g.getRadius2());
   }

   /**
    * Function that calls the calculation of eccentricity on all vertices and returns
    * an ArrayList containing all the centers of the graph.
    * @return centers
    */
//   public int getRadius(){
//      ArrayList<Vertex> comparables = this.getVertexList();
//      System.out.println("Radius begins: ");
//      int min = Integer.MAX_VALUE;
//
//      if (comparables.size() == 1) {
//         return 1;
//      }
//
//      for (Iterator<Vertex> iterator = comparables.iterator(); iterator.hasNext();){
//         Vertex vertex = iterator.next();
//         int temp = g.breadthFirst(vertex);
//         vertex.setVDistance(temp);
//         System.out.println(vertex.toString() + ", " + vertex.getVDistance() + ".\n");
//         if (temp < min){
//            min = temp;
//         }
//      }
//      return min;
//   }


   static public class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int distance;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      // This calls out the constructor with 3 input parameters, adds null's
      Vertex (String s) {

         this (s, null, null);
      }

      
      @Override
      public String toString() {
         return id;
      }

      public List<Arc> outEdges() {
         List<Arc> returnable = new ArrayList<>();
         if(this.first == null) return null;

         for (Arc v = this.first; v != null; v = v.next) {
            returnable.add(v);
         }
         return returnable;
      }

      public void setVInfo(int i){
         this.info = i;
      }

      public int getVInfo(){
         return this.info;
      }

      public void setVDistance(int i){
         this.distance = i;
      }

      public int getVDistance(){
         return this.distance;
      }

      /**
       * @param first Arc
       */
      public void setFirstArc(Arc first){
         this.first = first;
      }

      /**
       * @param next Vertex
       */
      public void setNextVertex(Vertex next){
         this.next = next;
      }


   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   static class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }


      /**
       * A simple getter for returning the target vertex object.
       * @return target;
       */
      public Vertex getToVert() {
         return target;
      }

      /**
       * @param target Vertex
       */
      public void setTargetVertex(Vertex target){
         this.target = target;
      }

      /**
       * @param next Arc
       */
      public void setNextArc(Arc next){
         this.next = next;
      }
   } 


   static class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      /**
       * @param a Vertex
       */
      public void setFirst(Vertex a){
         first = a;
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)          // 5, 4; 5 > 5*4/2
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      //references
      // https://www.youtube.com/watch?v=eEQ00TKw1Ww
      // https://www.youtube.com/watch?v=AfYqN3fGapc
      // https://www.youtube.com/watch?v=YbCn8d4Enos - radius
      // https://www.youtube.com/watch?v=O4LVAGV8tok - radius exapmple
      // Required for finding radius -> min eccen(G)
      /**
       * Method to calculate the eccentricity of a graph vertex.
       * Used source: http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * Used source: https://www.youtube.com/watch?v=YbCn8d4Enos
       * @param s vertex for which the eccentricity is calculated;
       * @return eccentricity;
       */
      public int breadthFirst(Vertex s){
         if (getVertexList() == null) throw new RuntimeException("No vertices defined!");
         if (!getVertexList().contains(s)) throw new RuntimeException("Wrong argument to traverse!");
         if (getVertexList().isEmpty()) throw new RuntimeException("There must be at least one vertex in graph");
         if (getVertexList().size() == 1) return 0;
         if (getVertexList().size() < 1) return 0;

         int eccentricity = 0;
         Iterator vit = getVertexList().iterator(); //Vertices to be worked on

         while (vit.hasNext()) {
            Vertex v = (Vertex)vit.next();                     // next element
            v.setVInfo(0);                                     // white - not processed
            v.setVDistance(0);                                 //Default distance is 0;
         }
         List vq = Collections.synchronizedList(new LinkedList());
         vq.add(s);                                            // add base vertex to the list
         s.setVInfo(1);                                        // gray - vertex v in progress
         while (vq.size() > 0) {
            Vertex v = (Vertex)vq.remove(0);                   // take the element at index 0 from vq and give it's value to v;
            //vertWork(v);
            v.setVInfo(2);                                     // black - vertex v is covered
            Iterator eit = v.outEdges().iterator();            // all edges (servad) connected to the vertex v
            while (eit.hasNext()) {
               Arc a = (Arc)eit.next();                        // take next element as Arc
               Vertex w = a.getToVert();
               if (w.getVInfo() == 0) {                        // if vertex is white (not processed)
                  w.setVDistance(v.getVDistance() + 1);        // give the vertex the appropriate distance from our base vertex, meaning +1 step.
                  if (eccentricity < w.getVDistance())         // if the vertex's distance from base is greater than in any other vertex's case, then:
                     eccentricity = w.getVDistance();          // give its value to eccentricity.
                  vq.add(w);                                   // add w to vq to be further processed.
                  w.setVInfo(1);                               // set w's status to "in progress".
               }
            }
         }

         return eccentricity;
      }

      public int getRadius2(){
         ArrayList<Vertex> comparables = this.getVertexList();
         //System.out.println("Radius begins: ");
         if (comparables.size() == 1) {
            return 1;
         }
         if (comparables.size() < 1) {
            return 0;
         }
         int min = Integer.MAX_VALUE;

         for (Iterator<Vertex> iterator = comparables.iterator(); iterator.hasNext();){
            Vertex vertex = iterator.next();
            int temp = this.breadthFirst(vertex);
            vertex.setVDistance(temp);
            //System.out.println(vertex.toString() + ", " + vertex.getVDistance() + ".\n");
            if (temp < min){
               min = temp;
            }
         }
         return min;
      }

      /** Work to do when traversing the graph */
      public void vertWork (Vertex v) {
         System.out.print (" " + v.toString());
      }

      /**
       * Method to get list of vertexes
       * @return ArrayList<Vertex>;
       */
      public ArrayList<Vertex> getVertexList() {
         ArrayList<Vertex> returnable = new ArrayList<>();

         for (Vertex v = this.first; v != null; v = v.next) {
            returnable.add(v);
         }
         return returnable;
      }

   }

} 

