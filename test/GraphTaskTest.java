import org.junit.Test;

import static org.junit.Assert.assertEquals;


/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void test1() {
      GraphTask.Graph g = new GraphTask.Graph("G");
      g.createRandomSimpleGraph(1,0);
      //System.out.println(g.toString());
      System.out.println("Test1 radius: " + g.getRadius2());
      assertEquals("Test1: ", 1, g.getRadius2());
   }

   @Test (timeout=20000)
   public void test2() {

      GraphTask.Graph g = new GraphTask.Graph("G");
      GraphTask.Vertex a = new GraphTask.Vertex("A");
      GraphTask.Vertex b = new GraphTask.Vertex("B");

      g.setFirst(a);
      a.setNextVertex(b);

      GraphTask.Arc ab = new GraphTask.Arc("AB");
      GraphTask.Arc ba = new GraphTask.Arc("BA");

      a.setFirstArc(ab);
      ab.setTargetVertex(b);

      b.setFirstArc(ba);
      ba.setTargetVertex(a);

      System.out.println("Test2 radius: " + g.getRadius2());
      assertEquals("Test2: ", 1, g.getRadius2());
   }

   @Test (timeout=20000)
   public void test3() {
      GraphTask.Graph g = new GraphTask.Graph("G");
      g.createRandomSimpleGraph(0,0);
      System.out.println("Test3 radius: " + g.getRadius2());
      assertEquals("Test3: ", 0, g.getRadius2());
   }

   @Test (timeout=20000)
   public void test4() {
      GraphTask.Graph g = new GraphTask.Graph("G");
      GraphTask.Vertex a = new GraphTask.Vertex("A");
      GraphTask.Vertex b = new GraphTask.Vertex("B");
      GraphTask.Vertex c = new GraphTask.Vertex("C");
      GraphTask.Vertex d = new GraphTask.Vertex("D");
      GraphTask.Vertex e = new GraphTask.Vertex("E");

      g.setFirst(a);
      a.setNextVertex(b);
      b.setNextVertex(c);
      c.setNextVertex(d);
      d.setNextVertex(e);

      GraphTask.Arc ab = new GraphTask.Arc("AB");
      GraphTask.Arc ba = new GraphTask.Arc("BA");
      GraphTask.Arc ac = new GraphTask.Arc("AC");
      GraphTask.Arc ca = new GraphTask.Arc("CA");
      GraphTask.Arc ad = new GraphTask.Arc("AD");
      GraphTask.Arc da = new GraphTask.Arc("DA");
      GraphTask.Arc bc = new GraphTask.Arc("BC");
      GraphTask.Arc cb = new GraphTask.Arc("CB");
      GraphTask.Arc bd = new GraphTask.Arc("BD");
      GraphTask.Arc db = new GraphTask.Arc("DB");
      GraphTask.Arc be = new GraphTask.Arc("BE");
      GraphTask.Arc eb = new GraphTask.Arc("EB");
      GraphTask.Arc cd = new GraphTask.Arc("CD");
      GraphTask.Arc dc = new GraphTask.Arc("DC");

      a.setFirstArc(ab);
      ab.setTargetVertex(b);

      a.setFirstArc(ab);
      ab.setTargetVertex(b);
      ab.setNextArc(ac);
      ac.setTargetVertex(c);
      ac.setNextArc(ad);
      ad.setTargetVertex(d);
      b.setFirstArc(ba);
      ba.setTargetVertex(a);
      ba.setNextArc(bc);
      bc.setTargetVertex(c);
      bc.setNextArc(bd);
      bd.setTargetVertex(d);
      bd.setNextArc(be);
      be.setTargetVertex(e);
      c.setFirstArc(ca);
      ca.setTargetVertex(a);
      ca.setNextArc(cb);
      cb.setTargetVertex(b);
      cb.setNextArc(cd);
      cd.setTargetVertex(d);
      d.setFirstArc(da);
      da.setTargetVertex(a);
      da.setNextArc(db);
      db.setTargetVertex(b);
      db.setNextArc(dc);
      dc.setTargetVertex(c);
      e.setFirstArc(eb);
      eb.setTargetVertex(b);

      System.out.println("Test4 radius: " + g.getRadius2());
      assertEquals("Test4: ", 1, g.getRadius2());
   }

   @Test (timeout=20000)
   public void test5() {
      GraphTask.Graph g = new GraphTask.Graph("G");
      GraphTask.Vertex a = new GraphTask.Vertex("A");
      GraphTask.Vertex b = new GraphTask.Vertex("B");
      GraphTask.Vertex c = new GraphTask.Vertex("C");
      GraphTask.Vertex d = new GraphTask.Vertex("D");
      GraphTask.Vertex e = new GraphTask.Vertex("E");

      g.setFirst(a);
      a.setNextVertex(b);
      b.setNextVertex(c);
      c.setNextVertex(d);
      d.setNextVertex(e);

      GraphTask.Arc ac = new GraphTask.Arc("AC");
      GraphTask.Arc ca = new GraphTask.Arc("CA");
      GraphTask.Arc ad = new GraphTask.Arc("AD");
      GraphTask.Arc da = new GraphTask.Arc("DA");
      GraphTask.Arc bc = new GraphTask.Arc("BC");
      GraphTask.Arc cb = new GraphTask.Arc("CB");
      GraphTask.Arc bd = new GraphTask.Arc("BD");
      GraphTask.Arc db = new GraphTask.Arc("DB");
      GraphTask.Arc be = new GraphTask.Arc("BE");
      GraphTask.Arc eb = new GraphTask.Arc("EB");
      GraphTask.Arc cd = new GraphTask.Arc("CD");
      GraphTask.Arc dc = new GraphTask.Arc("DC");
      GraphTask.Arc ea = new GraphTask.Arc("EA");
      GraphTask.Arc ae = new GraphTask.Arc("AE");

      a.setFirstArc(ae);
      ae.setTargetVertex(e);

      a.setFirstArc(ae);
      ae.setTargetVertex(e);
      ae.setNextArc(ac);
      ac.setTargetVertex(c);
      ac.setNextArc(ad);
      ad.setTargetVertex(d);
      b.setFirstArc(bc);
      bc.setTargetVertex(c);
      bc.setNextArc(bd);
      bd.setTargetVertex(d);
      bd.setNextArc(be);
      be.setTargetVertex(e);
      c.setFirstArc(ca);
      ca.setTargetVertex(a);
      ca.setNextArc(cb);
      cb.setTargetVertex(b);
      cb.setNextArc(cd);
      cd.setTargetVertex(d);
      d.setFirstArc(da);
      da.setTargetVertex(a);
      da.setNextArc(db);
      db.setTargetVertex(b);
      db.setNextArc(dc);
      dc.setTargetVertex(c);
      e.setFirstArc(eb);
      eb.setTargetVertex(b);
      eb.setNextArc(ea);
      ea.setTargetVertex(a);

      System.out.println("Test5 radius: " + g.getRadius2());
      assertEquals("Test5: ", 2, g.getRadius2());
   }

}

